<?php
namespace HelloWorld\Demo\Controller\Index;

class Test extends \Magento\Framework\App\Action\Action
{
    protected $resultFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\ResultFactory $resultFactory)
    {
        $this->resultFactory = $resultFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/test-category.html');

        return $redirect;
    }
}
