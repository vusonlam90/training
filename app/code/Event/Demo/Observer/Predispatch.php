<?php

namespace Event\Demo\Observer;

use \Psr\Log\LoggerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\RouterListInterface;

class Predispatch implements ObserverInterface
{
    protected $logger;
    protected $router;
    protected $_routerList;

    public function __construct(LoggerInterface $logger,
                                \Magento\Framework\App\RouterInterface $router,
                                \Magento\Framework\App\RouterListInterface $_routerList)
    {
        $this->logger = $logger;
        $this->router = $router;
        $this->_routerList = $_routerList;
    }

    public function execute(Observer $observer)
    {

        $routerList = $this->_routerList;
        foreach ($this->_routerList as $route) {
            echo get_class($route). "<br>";
        }

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('');
    }
}
