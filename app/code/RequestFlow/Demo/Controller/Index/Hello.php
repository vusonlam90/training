<?php
namespace RequestFlow\Demo\Controller\Index;

use Magento\UrlRewrite\Model\UrlRewriteFactory;

class Hello extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $_urlRewriteFactory;
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $urlRewriteModel = $this->_urlRewriteFactory->create();
/* set current store id */
$urlRewriteModel->setStoreId(1);
/* this url is not created by system so set as 0 */
$urlRewriteModel->setIsSystem(0);
/* unique identifier - set random unique value to id path */
$urlRewriteModel->setIdPath(rand(1, 100000));
/* set actual url path to target path field */
$urlRewriteModel->setTargetPath("www.example.com/customModule/customController/customAction");
/* set requested path which you want to create */
$urlRewriteModel->setRequestPath("www.example.com/xyz");
/* set current store id */
$urlRewriteModel->save();
//        echo "Hello World From RequestFlow";
//        exit;
    }
}
