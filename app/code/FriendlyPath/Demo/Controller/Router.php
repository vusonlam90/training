<?php
namespace FriendlyPath\Demo\Controller;

class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Router constructor.
     *
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     */
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory
    ) {
        $this->actionFactory = $actionFactory;
    }

    /**
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        $d = explode('/', $identifier, 3);

        if(isset($d[0]) && ($d[0] != 'frontName')) {
            return false;
        }

        $paramStr = '';
        if(isset($d[1])) {
            $paramStr = $d[1];
        }

        $params = [];

        if($paramStr) {
            $params = explode('-', $paramStr);
        }

        $params = ['frontName' => $params[0], 'actionPath' => $params[1], 'action' => $params[2]];

        $request->setModuleName('friendlypath')->setControllerName('index')->setActionName('index');
        if(count($params)) {
            $request->setParams($params);
        }

        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);

        return $this->actionFactory->create(
            'Magento\Framework\App\Action\Forward',
            ['request' => $request]
        );
    }
}
