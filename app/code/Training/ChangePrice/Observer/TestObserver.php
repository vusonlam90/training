<?php

namespace Training\ChangePrice\Observer;

use Magento\Catalog\Observer\Compare\BindCustomerLoginObserver;
use Magento\Framework\Event\ObserverInterface;

class TestObserver implements ObserverInterface
{
    protected $request;
    protected $logger;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger
    ){
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * Customer login bind process
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getData('request');
        $path = $request->getPathInfo();
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($path);
        $this->logger->debug($path);
        return $this;
    }
}

