<?php
//declare(strict_types=1);

namespace Training\ChangePrice\Block\Html;

/**
 * Html page footer block
 */
class Footer extends \Magento\Theme\Block\Html\Footer
{
    /**
     * @inheritdoc
     */
    public function getCopyright()
    {
            return 'customize copyright !!!';
    }
}
