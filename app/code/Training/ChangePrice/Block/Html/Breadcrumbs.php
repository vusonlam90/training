<?php

namespace Training\ChangePrice\Block\Html;

/**
 * Add crumb
 *
 * @param string $crumbName
 * @param array $crumbInfo
 * @return $this
 */

class Breadcrumbs extends \Magento\Theme\Block\Html\Breadcrumbs
{
    /**
     * Add crumb
     *
     * @param string $crumbName
     * @param array $crumbInfo
     * @return \Magento\Theme\Block\Html\Breadcrumbs
     */
    public function addCrumb($crumbName, $crumbInfo)
    {
        foreach ($this->_properties as $key) {
            if (!isset($crumbInfo[$key])) {
                $crumbInfo[$key] = null;
            }
        }

        if (!isset($this->_crumbs[$crumbName]) || !$this->_crumbs[$crumbName]['readonly']) {
            $this->_crumbs[$crumbName] = $crumbInfo;
        }

//        var_dump($crumbInfo);

        return $this;
    }
}

